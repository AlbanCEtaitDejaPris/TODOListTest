package test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

import classes.ServerH2;
import classes.TodoList;
import classes.TodoListElement;

public class TodoListElementTest {
	
	@Test
    public final void testGetTitle() {	
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		assertEquals("TestTitle", tle1.getTitle());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testSetTitle() {
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		tle1.setTitle("Test successful");
		assertEquals("Test successful", tle1.getTitle());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testGetDescription() {	
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		assertEquals("TestDescription", tle1.getDescription());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testSetDescription() {
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		tle1.setDescription("Test successful");
		assertEquals("Test successful", tle1.getDescription());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testGetDate() {	
		ServerH2.StartDb();
		Date dateBefore = new Date();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		
		try {
			Thread.sleep(1000); 
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Date dateAfter = new Date();

		assertTrue(tle1.getDate().compareTo(dateBefore)>=0);
		assertTrue(tle1.getDate().compareTo(dateAfter)<=0);
		System.out.println(tle1.getDate());

		ServerH2.StopConnection();
	}
	
	@Test
    public final void testGetTodoListId() {	
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		assertEquals(1, tle1.getTodoListId());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testGetState() {	
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		assertEquals(classes.State.TODO, tle1.getState()); // When created, the state of the TodoListeElement is TODO
		
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testSetState() {
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("TestTitle", "TestDescription", 1, true);
		tle1.setState(classes.State.ONGOING);
		assertEquals(classes.State.ONGOING, tle1.getState());
		
		ServerH2.StopConnection();
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
    public final void testGetFreeId() {
		ServerH2.StartDb();
		
		ArrayList<Double> listID = new ArrayList<Double>();
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT");
		try {
			while (resultSet.next()) {
				listID.add(Double.valueOf(resultSet.getInt("ID")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(!listID.contains((new TodoListElement("TestTitle", "TestDescription", 1, true)).getId()));
		
		ServerH2.StopConnection();
	}

}
