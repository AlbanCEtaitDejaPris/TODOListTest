package test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import classes.ServerH2;
import classes.TodoList;
import classes.TodoListElement;

public class TodoListTest {
	
	@Test
    public final void testGetTitle() {
		ServerH2.StartDb();
		
		ArrayList<TodoListElement> arrayTodo = new ArrayList<>();
		TodoList tl1 = new TodoList("Test passed", arrayTodo);
		assertEquals("Test passed", tl1.getTitle());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testSetTitle() {
		ServerH2.StartDb();
		
		ArrayList<TodoListElement> arrayTodo = new ArrayList<>();
		TodoList tl1 = new TodoList("Test", arrayTodo);
		tl1.setTitle("Test passed");
		assertEquals("Test passed", tl1.getTitle());
		
		ServerH2.StopConnection();
	}
	
	@Test
    public final void testGetSetListElement() {
		ServerH2.StartDb();
		TodoListElement tle1 = new TodoListElement("Ti", "Test passed", 1, true);
		TodoListElement tle2 = new TodoListElement("tle", "Test successful", 1, true);

		ArrayList<TodoListElement> arrayTodo = new ArrayList<>();
		arrayTodo.add(tle1);
		arrayTodo.add(tle2);

		TodoList tl1 = new TodoList("Test", arrayTodo);
		assertEquals(arrayTodo.size(), tl1.getListElement().size());

		boolean containsAll = true;
		for(TodoListElement tle: tl1.getListElement()) {
			if(!((tle.getTitle().equals("Ti") && tle.getDescription().equals("Test passed")) || (tle.getTitle().equals("tle") && tle.getDescription().equals("Test successful")))) {
				containsAll = false;
			}
		}
		assertTrue(containsAll);
		
		ServerH2.StopConnection();
	}
    
    
	@Test
    public final void testAddElement() {
		ServerH2.StartDb();
		
		TodoListElement tle1 = new TodoListElement("Ti", "Test passed", 1, true);
		TodoListElement tle2 = new TodoListElement("tle", "Test successful", 1, true);

		ArrayList<TodoListElement> arrayTodo = new ArrayList<>();
		
		TodoList tl1 = new TodoList("Test", arrayTodo);

		tl1.addElement(tle1);
		tl1.addElement(tle2);

		assertEquals(2, tl1.getListElement().size());

		boolean containsAll = true;
		for(TodoListElement tle: tl1.getListElement()) {
			if(!((tle.getTitle().equals("Ti") && tle.getDescription().equals("Test passed")) || (tle.getTitle().equals("tle") && tle.getDescription().equals("Test successful")))) {
				containsAll = false;
			}
		}
		assertTrue(containsAll);
		
		ServerH2.StopConnection();
    }
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
    public final void testGetFreeId() {
		ServerH2.StartDb();
		
		ArrayList<Double> listID = new ArrayList<Double>();
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLIST");
		try {
			while (resultSet.next()) {
				listID.add(Double.valueOf(resultSet.getInt("ID")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(!listID.contains((new TodoList()).getId())); // should not contain it
		
		ServerH2.StopConnection();
	}
	
}
