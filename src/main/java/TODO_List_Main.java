import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import classes.ServerH2;
import classes.TodoList;
import classes.TodoListElement;

public class TODO_List_Main {

	public static void main(String[] args) {
		ServerH2.StartDb();

		
		TodoListElement tle1 = new TodoListElement("Title", "Test", 1, true);
		ArrayList<TodoListElement> arrayTodo = new ArrayList<>();
		
		TodoList tl1 = new TodoList("Test", arrayTodo);
		
		tl1.addElement(tle1);

		
		
		
		ServerH2.StopConnection();
	}

}
