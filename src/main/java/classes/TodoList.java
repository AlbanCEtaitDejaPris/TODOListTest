package classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author atolsau
 *
 */
public class TodoList {
	
	private int id;
	private String title;
	private ArrayList<TodoListElement> listElement;

	
	/**
	 * Constructor
	 */
	public TodoList() {
		super();
		this.id = getFreeID();
		this.title = "";
		this.listElement = new ArrayList<TodoListElement>();
		ServerH2.executeUpdate("INSERT INTO TODOLIST VALUES(" + this.id +
														  ", '" + this.title + "');");
	}

	/**
	 * Constructor
	 * @param listElement
	 */
	public TodoList(String title, ArrayList<TodoListElement> listElement) {
		super();
		this.id = getFreeID();
		this.title = title;
		this.listElement = new ArrayList<TodoListElement>();
		ServerH2.executeUpdate("INSERT INTO TODOLIST VALUES(" + this.id +
				  ", '" + this.title + "');");
		this.setListElement(listElement);
	}
	
	/**
	 * Retreive the id of the TodoList.
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Set the if of the TodoList.
	 * @param int id
	 */
	private void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieve the Title of the TodoList.
	 * @return String
	 */
	public String getTitle() {
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLIST WHERE ID = " + this.id + ";");
		
		String str = "";
		try {
			while (resultSet.next()) {
				str += resultSet.getString("TITLE");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	/**
	 * Set the Title of the TodoList.
	 * @param String title
	 */
	public void setTitle(String title) {
		this.title = title;
		ServerH2.executeUpdate("UPDATE TODOLIST SET TITLE = '" + title + "' WHERE ID = " + this.id + ";");
	}

	/**
	 * Retrieve the ArrayList of TodoListElement of the TodoList.
	 * @return ArrayList<TodoListElement>
	 */
	public ArrayList<TodoListElement> getListElement() {
		ResultSet resultSet;
		ArrayList<TodoListElement> arrayTLE = new ArrayList<TodoListElement>();
		
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE TODOLIST_ID = " + this.id + ";");
		
		try {
			while (resultSet.next()) {
				arrayTLE.add(new TodoListElement(resultSet.getInt("ID"), resultSet.getString("TITLE"), resultSet.getString("DESCRIPTION"), this.id, false));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return arrayTLE;	
	}

	/**
	 * Set the ArrayList of TodoListElement of the TodoList.
	 * @param ArrayList<TodoListElement> listElement
	 */
	public void setListElement(ArrayList<TodoListElement> listElement) {

		for (TodoListElement tle : listElement) {
			this.addElement(tle);
		}
	}

	/**
	 * Add a TodoListElement at the end of the ArrayList of TodoListElement of the TodoList.
	 * @param TodoListElement tle
	 */
	public void addElement(TodoListElement tle) {
		this.listElement.add(tle);
		int id;
		String title;
		String description;
		Date date;
		classes.State state;

		id = tle.getId();
		title = tle.getTitle();
		description = tle.getDescription();
		date = tle.getDate();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
		state = tle.getState();
		
		ServerH2.executeUpdate("DELETE FROM TODOLISTELEMENT WHERE ID = " + id + ";");
		ServerH2.executeUpdate("INSERT INTO TODOLISTELEMENT VALUES(" + id + ", '" + title + "', '" + description + "', TIMESTAMP '" + simpleDateFormat.format(date) + "', '" + state.toString() + "', " + this.id + ");");
	}
	
	
	/**
	 * Retrieve a free ID for a TodoList from the Database
	 * @return int
	 * @throws SQLException
	 */
	private int getFreeID() {
		ArrayList<Double> listID = new ArrayList<Double>();
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLIST");
		try {
			while (resultSet.next()) {
				listID.add(Double.valueOf(resultSet.getInt("ID")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Double generatedId;
		
		// Check that the ID is not already used.
		do {
			generatedId = (Double) Math.random()*1000000000;
		} while (listID.contains(generatedId));
		
		return generatedId.intValue();
	}
}
