package classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author atolsau
 *
 */
public class ServerH2 {
	public static final String jdbcURL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
	public static final String username = "sa";
	public static final String password = "1234";
	public static Connection connection = null; 

	/**
	 * Start the connection to the H2 server (mandatory to use the other ServerH2 functions).
	 */
	public static Connection StartDb() {
		Statement statement;
		ResultSet resultSet = null;
		
		try {
			connection = DriverManager.getConnection(ServerH2.jdbcURL, ServerH2.username, ServerH2.password);
			System.out.println("Connected to H2 database");
			
			statement = connection.createStatement();
            FileReader fr = new FileReader("db.sql") ;
            BufferedReader br = new BufferedReader(fr) ;
            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
            	statement.execute(strCurrentLine);                
            }
            
			System.out.println("Database initialized");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
	/**
	 * Execute a query with a return (select)
	 * @param String query
	 * @return ResultSet
	 */
	public static ResultSet executeQuery(String query) {
		Statement statement;
		ResultSet resultSet = null;
		
		try {			
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultSet;
	}
	
	/**
	 * Execute a query with no return (update, insert, etc.)
	 * @param String query
	 */
	public static void executeUpdate(String query) {
		Statement statement;
		
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Close the connection to the H2 server.
	 */
	public static void StopConnection() {
		try {
			connection.close();
			System.out.println("Disconnected from H2 database");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
}
