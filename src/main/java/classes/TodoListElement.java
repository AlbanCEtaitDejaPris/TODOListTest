package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * @author atolsau
 *
 */
public class TodoListElement {
	
	private int id;
	private String title;
	private String description;
	private Date date;
	private classes.State state;

	/**
	 * Constructor
	 * @param String description, int idTodoList, boolean createInDb
	 */
	public TodoListElement(String title, String description, int idTodoList, boolean createInDb) {
		super();
		this.id = this.getFreeID();
		this.title = title;
		this.description = description;
		this.date = new Date();
		this.state = classes.State.TODO;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
		
		if (createInDb) {
			ServerH2.executeUpdate("INSERT INTO TODOLISTELEMENT VALUES('" + this.id +
															   "', '" + this.title +
															   "', '" + this.description +
															   "', TIMESTAMP '" + simpleDateFormat. format(this.date) +
															   "', '" + this.state.toString() +
															    "', " + idTodoList + ");");
		}
	}
	
	/**
	 * Constructor
	 * @param int id, String description, int idTodoList, boolean createInDb
	 */
	protected TodoListElement(int id, String title, String description, int idTodoList, boolean createInDb) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.date = new Date();
		this.state = classes.State.TODO;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
		
		if (createInDb) {
			ServerH2.executeUpdate("INSERT INTO TODOLISTELEMENT VALUES('" + this.id +
															   "', '" + this.title +
															   "', '" + this.description +
															   "', TIMESTAMP '" + simpleDateFormat. format(this.date) +
															   "', '" + this.state.toString() +
															    "', " + idTodoList + ");");
		}
	}

	/**
	 * Retrieve the title of the TodoListElement.
	 * @return String
	 */
	public String getTitle() {
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE ID = " + this.id + ";");

		String str = "";
		try {
			while (resultSet.next()) {
				str += resultSet.getString("TITLE");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}

	/**
	 * Set the title of the TodoListElement.
	 * @param String description
	 */
	public void setTitle(String title) {
		this.title = title;
		ServerH2.executeUpdate("UPDATE TODOLISTELEMENT SET TITLE = '" + title + "' WHERE ID = " + this.id + ";");
	}
	
	/**
	 * Retrieve the description of the TodoListElement.
	 * @return String
	 */
	public String getDescription() {
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE ID = " + this.id + ";");

		String str = "";
		try {
			while (resultSet.next()) {
				str += resultSet.getString("DESCRIPTION");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return str;
	}
	
	/**
	 * Set the description of the TodoListElement.
	 * @param String description
	 */
	public void setDescription(String description) {
		this.description = description;
		ServerH2.executeUpdate("UPDATE TODOLISTELEMENT SET DESCRIPTION = '" + description + "' WHERE ID = " + this.id + ";");
	}

	/**
	 * Retrieve the ID of the TodoListElement
	 * @return int
	 */
	public int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieve the creation date of the TodoListElement.
	 * @return Date
	 */
	public Date getDate() {
		ResultSet resultSet;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE ID = " + this.id + ";");

		Date date = null;
		try {
			while (resultSet.next()) {
				date = simpleDateFormat.parse(resultSet.getString("DATE"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date;
	}

	private void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Retrieve the TodoList ID of the TodoListElement.
	 * @return int
	 */
	public int getTodoListId() {
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE ID = " + this.id + ";");
		
		int idTodoList = -1;
		try {
			while (resultSet.next()) {
				idTodoList = resultSet.getInt("TODOLIST_ID");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return idTodoList;
	}
	
	/**
	 * Retrieve the state of a TodoListElement.
	 * @return classes.State
	 */
	public classes.State getState() {
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT WHERE ID = " + this.id + ";");
		
		String strState = "";
		classes.State state = null;
		try {
			while (resultSet.next()) {
				strState += resultSet.getString("STATE");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		switch (strState) {
			case "TODO":
				state = classes.State.TODO;
				break;
			case "ONGOING":
				state = classes.State.ONGOING;
				break;
			case "DONE":
				state = classes.State.DONE;
				break;
			default:
				System.out.println(new Exception("The TODO list element has a wrong STATE: " + strState + "."));
				break;
		}
		
		return state;
	}

	
	/**
	 * Set the state of the TodoListElement.
	 * @param classes.State state
	 */
	public void setState(classes.State state) {
		this.state = state;
		ServerH2.executeUpdate("UPDATE TODOLISTELEMENT SET STATE = '" + state.toString() + "' WHERE ID = " + this.id + ";");
	}

	/**
	 * Retrieve a free ID for a TodoListElement from the Database.
	 * @return int
	 * @throws SQLException
	 */
	private int getFreeID() {
		ArrayList<Double> listID = new ArrayList<Double>();
		ResultSet resultSet;
		resultSet = ServerH2.executeQuery("SELECT * FROM TODOLISTELEMENT");
		try {
			while (resultSet.next()) {
				listID.add(Double.valueOf(resultSet.getInt("ID")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Double generatedId;
		
		// Check that the ID is not already used.
		do {
			generatedId = (Double) Math.random()*1000000000;
		} while (listID.contains(generatedId));
		
		return generatedId.intValue();
	}
	
}
